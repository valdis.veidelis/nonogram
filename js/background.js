/* 
 * Contains data of the background image to be revealed.
 */

window.aD = window.aD || {};

aD.Background = function ( args ) {
	this.data = [
		[1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0],
		[0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
		[1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
		[1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1],
		[1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
		[0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1],
		[0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1],
		[0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1],
		[1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1],
		/*[0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1],
		[1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
		[0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1],
		[1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],*/
		[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1],
		[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1]
		
	];
	this.padding = {
		'left': [],
		'top': []
	};
	this.calculatePaddingData();
}

/*	Padding top
 *  0|----------|
 * |-|----------|
 * |l|			|
 * |e|			|
 * |f|			|
 * |t|			|
 * |-|----------|
 */

aD.Background.prototype.calculatePaddingDataLeft = function () {
	var padding_data_left = [],
		coterminous_count_in_row = 0;
	for( var i = 0; i < this.data.length; i++ ) {
		padding_data_left[i] = [];
		for( var j = this.data[i].length - 1; j >= 0; j-- ) {
			if( this.data[i][j] === 1 ) {
				coterminous_count_in_row++;
			}
			if( !this.data[i][j - 1] && coterminous_count_in_row !== 0 ) {
				padding_data_left[i].push( coterminous_count_in_row );
				coterminous_count_in_row = 0;
			}
		}
	}
	return padding_data_left;
}

aD.Background.prototype.calculatePaddingData = function () {
	this.padding.left = this.calculatePaddingDataLeft();
	this.data = this.data.transpose();
	this.padding.top = this.calculatePaddingDataLeft();
	this.data = this.data.transpose();
};

aD.Background.prototype.getPaddingWidth = function () {
	var padding_width = 0;
	for( var i = this.padding.left.length - 1; i >= 0; i-- ) {
		if( this.padding.left[i].length > padding_width ) {
			padding_width = this.padding.left[i].length;
		}
	}
	return padding_width * aD.TILE_WIDTH;
};

aD.Background.prototype.getPaddingHeight = function () {
	var padding_height = 0;
	for( var i = this.padding.top.length - 1; i >= 0; i-- ) {
		if( this.padding.top[i].length > padding_height ) {
			padding_height = this.padding.top[i].length;
		}
	}
	return padding_height * aD.TILE_HEIGHT;
}