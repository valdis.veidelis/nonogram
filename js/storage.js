/*
 * localStorage wrapper.
 */

var aD = aD || {};

aD.storage = {
	'get': function ( key ) {
		return JSON.parse( localStorage.getItem(key) );
	},
	'set': function ( key, value ) {
		localStorage.setItem( key, JSON.stringify(value) );
		return this;
	},
	'remove': function ( key ) {
		localStorage.removeItem( key );
		return this;
	},
	'clear': function () {
		localStorage.clear();
		return this;
	}
};