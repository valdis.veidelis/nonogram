
window.aD = window.aD || {};

aD.utils = {};

/*
 * @param two one-dimensional arrays
 */
aD.utils.areArraysEqual = function ( array_1, array_2 ) {
	if( array_1.length !== array_2.length ) {
		return false;
	}
	for( var i = array_1.length - 1; i >= 0; i-- ) {
		if( array_1[i] !== array_2[i] ) {
			return false;
		}
	}
	return true;
}