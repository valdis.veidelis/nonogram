( function ( w, m, d, $, undefined ) {
	
	w.requestAnimationFrame = w.requestAnimationFrame ||
        w.oRequestAnimationFrame || w.mozRequestAnimationFrame ||
        w.webkitRequestAnimationFrame || w.msRequestAnimationFrame;
	
	w.aD = w.aD || {};
	
	aD.Nonogram = function ( args ) {
		this.background = new aD.Background();
		this.highlightPosition = null; // tile on grid to highlight
		this.padding = {
			'left': [],
			'top': []
		};
		this.tiles = [];
		this.totalActiveTilesCount = 0;
		this.currentActiveTilesCount = 0;
		this.initialize();
	};

	aD.mouse = { 'x': 0, 'y': 0, '_x': 0, '_y': 0 };
	aD.mouse.isDragging = false;
	aD.mouse.dragStartPosition = { '_x': null, '_y': null };

	aD.updateMouseCoordinates = function ( event ) {
		aD.mouse.x = event.pageX - this.offsetLeft - 6;
		aD.mouse.y = event.pageY - this.offsetTop - 6;
		// calculate 2d coords for grid
		aD.mouse._x = aD.mouse.x - aD.mouse.x % aD.TILE_WIDTH;
		if( aD.mouse._x > aD.canvas.width - aD.TILE_WIDTH ) aD.mouse._x = aD.canvas.width - aD.TILE_WIDTH;
		aD.mouse._y = aD.mouse.y - aD.mouse.y % aD.TILE_HEIGHT;
		if( aD.mouse._y > aD.canvas.height - aD.TILE_HEIGHT ) aD.mouse._y = aD.canvas.height - aD.TILE_HEIGHT;
	};

	aD.Nonogram.prototype.adjustCanvasDimensions = function () {
		aD.canvas.height = this.background.data.length * aD.TILE_HEIGHT + this.background.getPaddingHeight();
		aD.canvas.width = this.background.data[0].length * aD.TILE_WIDTH + this.background.getPaddingWidth();
	}

	aD.Nonogram.prototype.initialize = function () {
		this.adjustCanvasDimensions();
		for( var i = this.background.data.length - 1; i >= 0; i-- ) {
			this.tiles[i] = [];
			for( var j = this.background.data[i].length - 1; j >= 0; j-- ) {
				this.tiles[i][j] = new aD.Tile({
					'game': this,
					//'status': this.background.data[i][j],
					'x': j,
					'y': i
				});
				if( this.background.data[i][j] === aD.Tile.statuses.FULL ) {
					this.totalActiveTilesCount++;
				}
			}
		}
		this.calculatePaddingData();
		this.handleInput();
		this.loop();
		console.log( this.totalActiveTilesCount );
	};

	aD.Nonogram.prototype.forEachTile = function( callback ) {
		for ( i = this.tiles.length - 1; i >= 0; i-- ) {
			for( j = this.tiles[i].length - 1; j >= 0; j-- ) {
				callback.call( this.tiles[i][j], this.tiles[i][j] );
			}
		}
	};

	aD.Nonogram.prototype.loop = function () {
		var self = this;
		w.requestAnimationFrame.call( w, function () {
			aD.Nonogram.prototype.loop.call( self );
		});
		this.draw();
	};

	/*
	 * Calculate left padding gata for user
	 * clicked tiles, not actual background data.
	 */
	aD.Nonogram.prototype.calculatePaddingDataLeft = function () {
		var padding_data_left = [],
			coterminous_count_in_row = 0;
		for( var i = 0; i < this.tiles.length; i++ ) {
			padding_data_left[i] = [];
			for( var j = this.tiles[i].length - 1; j >= 0; j-- ) {
				if( this.tiles[i][j].status === 1 ) {
					coterminous_count_in_row++;
				}
				if( (!this.tiles[i][j - 1] || this.tiles[i][j - 1].status !== 1) && coterminous_count_in_row !== 0 ) {
					padding_data_left[i].push( coterminous_count_in_row );
					coterminous_count_in_row = 0;
				}
			}
		}
		return padding_data_left;
	}

	aD.Nonogram.prototype.calculatePaddingData = function () {
		this.padding.left = this.calculatePaddingDataLeft();
		this.tiles = this.tiles.transpose();
		this.padding.top = this.calculatePaddingDataLeft();
		this.tiles = this.tiles.transpose();
	};
	
	/*
	 * returns true if selected tile pattern 
	 * conforms to the one displayed on the left
	 */
	aD.Nonogram.prototype.isRowFilled = function ( rowId ) {
		return aD.utils.areArraysEqual(
			this.padding.left[ rowId ],
			this.background.padding.left[ rowId ]
		);
	};
	
	/*
	 * returns true if selected tile pattern 
	 * conforms to the one displayed on the top
	 */
	aD.Nonogram.prototype.isColumnFilled = function ( columnId ) {
		return aD.utils.areArraysEqual(
			this.padding.top[ columnId ],
			this.background.padding.top[ columnId ]
		);
	}

	aD.Nonogram.prototype.handleInput = function () {
		
		var self = this;

		$('#nonogram').on({
			'mousemove': ( function () {
				return function( event ) {
					aD.updateMouseCoordinates.call( this, event );
					self.highlightPosition = { 'x': aD.mouse._x, 'y': aD.mouse._y  };
					// iekrāsot no peles x līdz 0(x) un peles y līdz 0(y)
					if( aD.mouse.dragStartPosition._x !== null && aD.mouse.dragStartPosition._y !== null ) {
						aD.mouse.isDragging = true;
						console.log( Math.abs( aD.mouse.dragStartPosition._x - aD.mouse._x ));
					}
				}
			}()),
			'click': function ( event ) {
				
				aD.updateMouseCoordinates.call( this, event );
				
				self.forEachTile( function () {
					if( self.tiles[i][j].x === ( aD.mouse._x - self.background.getPaddingWidth() ) / aD.TILE_WIDTH 
					 && self.tiles[i][j].y === ( aD.mouse._y - self.background.getPaddingHeight() ) / aD.TILE_HEIGHT ) {
						self.tiles[i][j].switchStatus();
					}
				});
			
				self.calculatePaddingData();
				
			},
			'mousedown': function () {
				aD.updateMouseCoordinates.call( this, event );
				console.log('MOUSEDOWN');
				aD.mouse.dragStartPosition._x = aD.mouse._x;
				aD.mouse.dragStartPosition._y = aD.mouse._y;
				console.log(aD.TILE_WIDTH);
				return false; // http://stackoverflow.com/questions/3799686/clicking-inside-canvas-element-selects-text
			},
			'mouseup': function () {
				aD.mouse.dragStartPosition = { '_x': null, '_y': null };;
				console.log('MOUSEUP');
			}
			// TODO iezīmēt, turot peles pogu un velkot
		});
	
		$('#tile-hover-highlight').change( function () {
			aD.TILE_HOVER_HIGHLIGHT = !aD.TILE_HOVER_HIGHLIGHT;
			aD.storage.set( 'play.estrika.com-nonogram-tile_hover_highlight', aD.TILE_HOVER_HIGHLIGHT );
		});
		
		$('#tile-size').change( function () {
			aD.TILE_WIDTH = $('#tile-size').val();
			aD.TILE_HEIGHT = $('#tile-size').val();
			aD.storage.set( 'play.estrika.com-nonogram-tile_size', $('#tile-size').val() );
			self.adjustCanvasDimensions();
		});
		
	};

	aD.Nonogram.prototype.draw = function () {
		
		var i, j, self = this;
		
		aD.context.fillStyle = '#ffffff';
		aD.context.fillRect( 0, 0, aD.canvas.width, aD.canvas.height );
		
		aD.context.font = '12pt Times New Roman';
		aD.context.textAlign = 'center';
		aD.context.strokeStyle = '#000000';
		aD.context.fillStyle = '#ff0000';
		
		// draw left padding
		for( i = this.background.padding.left.length - 1; i >= 0; i-- ) {
			if( this.isRowFilled(i) ){
				aD.context.strokeStyle = aD.colors.LINE_COMPLETED;
			} else {
				aD.context.strokeStyle = aD.colors.LINE_NOT_COMPLETED;
			}
			for( j = 0; j < this.background.padding.left[i].length; j++ ) {
				aD.context.strokeText(
					this.background.padding.left[i][j], 
					this.background.getPaddingWidth() - (j + 1) * aD.TILE_WIDTH + 12, 
					this.background.getPaddingHeight() + (i + 1) * aD.TILE_HEIGHT - 8
				);
			}
		}
		
		// draw top padding
		for( i = this.background.padding.top.length - 1; i >= 0; i-- ) {
			if( this.isColumnFilled(i) ){
				aD.context.strokeStyle = aD.colors.LINE_COMPLETED;
			} else {
				aD.context.strokeStyle = aD.colors.LINE_NOT_COMPLETED;
			}
			for( j = 0; j <= this.background.padding.top[i].length - 1; j++ ) {
				aD.context.strokeText(
					this.background.padding.top[i][j],
					this.background.getPaddingWidth() + i * aD.TILE_WIDTH + 12,
					this.background.getPaddingHeight() - j * aD.TILE_HEIGHT - 8
				);
			}
		}
		
		// draw each tile
		this.forEachTile( aD.Tile.prototype.draw );
	
		// horizontal lines
		aD.context.strokeStyle = '#dddddd';
		aD.context.lineWidth = 1;
		for( i = this.background.data.length - 2; i >= 0; --i ) {
			aD.context.beginPath();
			aD.context.moveTo(
				this.background.getPaddingWidth(),
				(i + 1) * aD.TILE_HEIGHT + this.background.getPaddingHeight()
			);
			aD.context.lineTo(
				aD.canvas.width,
				(i + 1) * aD.TILE_HEIGHT + this.background.getPaddingHeight()
			);
			aD.context.stroke();
		}
	
		// vertical lines
		aD.context.strokeStyle = '#dddddd';
		aD.context.lineWidth = 1;
		for( i = this.background.data[0].length - 1; i > 0; --i ) {
			aD.context.beginPath();
			aD.context.moveTo(
				this.background.getPaddingWidth() + i * aD.TILE_WIDTH,
				this.background.getPaddingHeight()
			);
			aD.context.lineTo(
				this.background.getPaddingWidth() + i * aD.TILE_WIDTH,
				this.background.getPaddingHeight() + this.background.data.length * aD.TILE_HEIGHT
			);
			aD.context.stroke();
		}
		
		if( aD.TILE_HOVER_HIGHLIGHT && this.highlightPosition !== null ) {
			this.forEachTile( function () {
				if( ( ( this.x <= ( self.highlightPosition.x - self.background.getPaddingWidth() ) / aD.TILE_WIDTH  
				 && this.y === ( self.highlightPosition.y - self.background.getPaddingHeight() ) / aD.TILE_HEIGHT )
				 || ( this.x === ( self.highlightPosition.x - self.background.getPaddingWidth() ) / aD.TILE_WIDTH  
				 && this.y <= ( self.highlightPosition.y - self.background.getPaddingHeight() ) / aD.TILE_HEIGHT ) )
				 && this.status !== aD.Tile.statuses.FULL )  {
					aD.context.fillStyle = '#dff2ff';
					aD.context.fillRect(
						this.x * aD.TILE_WIDTH + 3 + self.background.getPaddingWidth(),
						this.y * aD.TILE_HEIGHT + 3 + self.background.getPaddingHeight(),
						aD.TILE_WIDTH - 6, aD.TILE_HEIGHT - 6
					);
				}
			});
		}
		
		if( aD.mouse.isDragging === true && aD.mouse.dragStartPosition._x && aD.mouse.dragStartPosition._y ) {
			aD.context.fillStyle = 'red';
			aD.context.fillRect(
				Math.min( aD.mouse._x, aD.mouse.dragStartPosition._x ),
				Math.min( aD.mouse._y, aD.mouse.dragStartPosition._y ),
				Math.abs( aD.mouse._x - aD.mouse.dragStartPosition._x ),
				Math.abs( aD.mouse._y - aD.mouse.dragStartPosition._y )
			);
		}
		
		if( aD.mouse._x >= this.background.getPaddingWidth() 
		 && aD.mouse._y >= this.background.getPaddingHeight() ) {
			aD.context.lineWidth = 1;
			aD.context.strokeStyle = 'blue';
			aD.context.strokeRect(
				aD.mouse._x,
				aD.mouse._y,
				aD.TILE_WIDTH - 1,
				aD.TILE_HEIGHT - 1
			);
		}
		
		/*aD.context.lineWidth = 1;
		aD.context.strokeStyle = 'orange';
		aD.context.beginPath();
		aD.context.moveTo( aD.mouse._x, 0);
		aD.context.lineTo( aD.mouse._x, aD.canvas.height );
		aD.context.stroke();*/
	
	};

	w.addEventListener('load', function () {
		var _tile_hover_highlight = aD.storage.get('play.estrika.com-nonogram-tile_hover_highlight'),
			_tile_size = aD.storage.get('play.estrika.com-nonogram-tile_size');
		if( _tile_hover_highlight !== null ) {
			$('#tile-hover-highlight').prop('checked', _tile_hover_highlight);
			aD.TILE_HOVER_HIGHLIGHT = _tile_hover_highlight;
		}
		if( _tile_size !== null ) {
			$('#tile-size').val( _tile_size );
			aD.TILE_HEIGHT = aD.TILE_WIDTH = _tile_size;
		}
		aD.canvas = document.getElementById('nonogram');
		aD.context = aD.canvas.getContext('2d');
		new aD.Nonogram();
	}, false );
	

} ( window, Math, Date, jQuery ) );