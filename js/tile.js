aD = aD || {};

aD.Tile = function ( args ) {
	this.game = args.game;
	this.x = args.x;
	this.y = args.y;
	this.status = args.status || aD.Tile.statuses.UNKNOWN;
};

aD.Tile.statuses = {
	'UNKNOWN': 0,
	'FULL': 1,
	'EMPTY': 2
}

aD.Tile.prototype.switchStatus = function () {
	this.status = ( ++this.status > 2 ) ? 0 : this.status;
	if( this.status === aD.Tile.statuses.FULL ) {
		this.game.currentActiveTilesCount++;
	} else {
		this.game.currentActiveTilesCount--;
	}
}

aD.Tile.prototype.draw = function () {

	if( this.status === aD.Tile.statuses.FULL ) {
		aD.context.fillStyle = 'black';
		aD.context.fillRect(
			this.x * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			this.y * aD.TILE_HEIGHT + this.game.background.getPaddingHeight(),
			aD.TILE_WIDTH,
			aD.TILE_HEIGHT
		);
	} else if( this.status === aD.Tile.statuses.EMPTY ) {
		aD.context.strokeStyle = '#aaaaaa';
		aD.context.beginPath();
		aD.context.moveTo(
			this.x * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			this.y * aD.TILE_WIDTH + this.game.background.getPaddingHeight()
		);
		aD.context.lineTo(
			(this.x + 1) * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			(this.y + 1) * aD.TILE_WIDTH + this.game.background.getPaddingHeight()
		);
		aD.context.moveTo(
			(this.x + 1) * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			this.y * aD.TILE_WIDTH + this.game.background.getPaddingHeight()
		);
		aD.context.lineTo(
			this.x * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			(this.y + 1) * aD.TILE_WIDTH + this.game.background.getPaddingHeight()
		);
		aD.context.stroke();
	} else {
		aD.context.fillStyle = 'white';
		aD.context.fillRect(
			this.x * aD.TILE_WIDTH + this.game.background.getPaddingWidth(),
			this.y * aD.TILE_HEIGHT + this.game.background.getPaddingHeight(),
			aD.TILE_WIDTH,
			aD.TILE_HEIGHT
		);
	}

		

}