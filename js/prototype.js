/*
 * [				[
 *	[0, 1],  --\	 [0, 0],
 *	[0, 1]   --/	 [1, 1]
 * ]				]
 */
Array.prototype.transpose = function() {
	var arr = this;
	return Object.keys( arr[0] ).map( function(i) {
		return arr.map( function (j) {
			return j[i];
		})
	});
};