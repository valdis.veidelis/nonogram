var aD = aD || {};

aD.TILE_WIDTH = 32;
aD.TILE_HEIGHT = 32;
aD.TILE_HOVER_HIGHLIGHT = false;

aD.colors = {
	'LINE_NOT_COMPLETED': '#000000',
	'LINE_COMPLETED': '#cccccc'
}